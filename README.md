Product Name
---------------
Stack - Responsive Bootstrap 4 Admin Template


Product Description
-------------------
Stack admin includes 7 pre-built templates with organized folder structure, clean & commented code,
1500+ pages, 500+ components, 100+ charts, 50+ advance cards (widgets), searchable navigation, RTL and incredible support.

Change Log
----------
Read CHANGELOG.md file